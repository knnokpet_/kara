//
//  main.m
//  gitFlow
//
//  Created by bp057 on 2015/03/25.
//  Copyright (c) 2015年 bp057. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
